# Getting Started
---

## Overview

The Nune pricing API allows an insurance provider to find the optimum price-point for any given quotation proposal profile.

The basic steps are as follows;

1. Set up an account
2. Use our API to set up details about the product
3. Submit the details of each quotation to our API and use the premium returned by the API to present to the proposer
4. If the policy is purchased, let us know
5. (future) Let us know if a claim is made and then let us know the final settlement
6. That's it.

## Create an account

Please contact us at [hi@nune.team](mailto:hi@nune.team) to get started

## Using the API

### Training the model

The first step is to use our `/model/train` API to set up some information about rating factor groups. This would include any rating factor values that shares the same effect on the quote.

#### An example;

A rating factor vehicle_engine_type on a motor policy has four possible values with the following effects on the quotation premium;
```
+----------+-------------+
| Gasoline | 5% loading  |
| Diesel   | -           |
| Electric | 5% discount |
| Hybrid   | 5% discount |
+----------+-------------+
```
In this instance you would create a group for `Electric` and `Hybrid` as their pricing affect is the same.

Note: These values are used as a starting point by the Nune AI pricing engine, over time groups are reassigned automatically as required

### Submitting a quotation

When a quotation is made via your digital platform, before presenting the price back to the proposer your system first sends the quotation details to the Nune AI `/model/predict` API. This returns a price based on the provided factors that is to be presented to the proposer in place of your system generated quote premium. The premium returned may be the same as the premium provided, or it may differ either up or down. Over time, the pricing engine will determine the optimal price and adjust as required.

### Informing us of the outcome

If a quote is purchased, use the `/outcome` API to inform us.

### Inform us of a claim

Use the `/claim` API to submit details of FNOL and final settlement.









