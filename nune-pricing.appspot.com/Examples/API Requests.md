
# Example API Requests
---

### `/model/train`

```
{
  "identifier": "Engine1",
  "type": "UPDATE_GROUP",
  "feature": [
	  	{"name": "Engine", "value": "Electric"},
	  	{"name": "Engine", "value": "Hybrid"},
	  	{"name": "Engine", "value": "CNG"}
  	]

}
```

### `/model/predict`

```

{
  "customerId": "C1001",
  "quoteId": "QT_1003",
  "quoteExpiry": "2020-10-01",
  "price": 105.04,
  "feature": [
	  	{"name": "Engine", "value": "Electric"},
	  	{"name": "MoreThan7Seats", "value": "true"},
	  	{"name": "DriverAge", "value": "47"}
  	]
}

```

### `/outcome`

```
{
  "quoteId": "QT_1003",
  "result": "PURCHASED"
}
```


